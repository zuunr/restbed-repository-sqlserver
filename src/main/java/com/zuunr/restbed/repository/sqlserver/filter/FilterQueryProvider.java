/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.filter;

import java.util.List;
import java.util.regex.Matcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * <p>The FilterQueryProvider is responsible for translating filter techniques 
 * used in API to a SQLServer json friendly query.</p>
 * 
 * <p>The implementation supports the following 6 API filtering techniques:</p>
 * 
 * <ul>
 * <li>myParam={value1} - Where my parameter must be equal to {value1}</li>
 * <li>myParam={value1},{value2}... - Where my parameter must be equal to any of the provided values</li>
 * <li>myParam=[{value1},{value2}] - Where my parameter must be greater or equals
 *  to {value1} and less than or equals to {value2}</li>
 * <li>myParam=({value1},{value2}) - Where my parameter must be greater than
 *  {value1} and less than {value2}</li>
 * <li>myParam=[{value1},{value2}) - Where my parameter must be greater or equals
 *  to {value1} and less than {value2}</li>
 * <li>myParam=({value1},{value2}] - Where my parameter must be greater than
 *  {value1} and less than or equals to {value2}</li>
 * </ul>
 * 
 * <p>A parameter must only be added once, e.g. only one of the above techniques
 * must be applied.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class FilterQueryProvider {
    
    private RangeFilterCreator rangeFilterCreator;
    private FixedValueFilterCreator fixedValueFilterCreator;
    
    @Autowired
    public FilterQueryProvider(
            RangeFilterCreator rangeFilterCreator,
            FixedValueFilterCreator fixedValueFilterCreator) {
        this.rangeFilterCreator = rangeFilterCreator;
        this.fixedValueFilterCreator = fixedValueFilterCreator;
    }

    /**
     * <p>Creates the json-friendly WHERE part of the SQL given the filter parameters,
     * which could be used to filter out elements not matching the selected filter.</p>
     * 
     * @param filter is an {@link JsonArray} containing the different filters
     * @return a string containing the WHERE part
     */
    public String getFilterQuery(JsonArray filter) {
        List<JsonObject> filterAsList = filter.asList(JsonObject.class);

        if (filterAsList.isEmpty()) {
            return "";
        }

        return createQuery(filterAsList);
    }

    private String createQuery(List<JsonObject> filterAsList) {
        StringBuilder filterQuery = new StringBuilder();
        filterQuery.append("WHERE ");

        for (JsonObject filterObject : filterAsList) {
            String path = filterObject.get("path").getValue(String.class);
            String expression = filterObject.get("expr", "").getValue(String.class);

            filterQuery.append(createFilter(path, expression));
            filterQuery.append(" AND ");
        }

        filterQuery.delete(filterQuery.length() - 5, filterQuery.length());

        return filterQuery.toString();
    }

    private String createFilter(String path, String filterExpression) {
        Matcher matcher = RangeFilterCreator.pattern.matcher(filterExpression);

        if (matcher.matches()) {
            return rangeFilterCreator.createFilter(matcher, path);
        }
        
        return fixedValueFilterCreator.createFilter(filterExpression, path);
    }
}
