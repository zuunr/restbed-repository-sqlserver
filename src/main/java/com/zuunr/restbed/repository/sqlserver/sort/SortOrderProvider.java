/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.sort;

import java.util.List;

import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * <p>The SortOrderProvider is responsible for translating sort order techniques 
 * used in API to a SQLServer json friendly query.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class SortOrderProvider {

    private static final String DEFAULT_ORDER_BY = "ORDER BY _id";

    /**
     * <p>Creates the ORDER BY part of the sql given the input {@link JsonArray}
     * containing parameters and their ascending/descending order.</p>
     * 
     * @param sortOrder is a {@link JsonArray} containing the parameter sort order
     * @return a string containing the order by part
     */
    public String getSortOrderQuery(JsonArray sortOrder) {
        List<JsonObject> sortOrderAsList = sortOrder.asList(JsonObject.class);

        if (sortOrderAsList.isEmpty()) {
            return DEFAULT_ORDER_BY;
        }

        return createQuery(sortOrderAsList);
    }

    private String createQuery(List<JsonObject> sortOrderAsList) {
        StringBuilder sortOrderQuery = new StringBuilder();
        sortOrderQuery.append("ORDER BY ");

        for (JsonObject orderObject : sortOrderAsList) {
            String field = orderObject.get("param").getValue(String.class);
            boolean ascending = orderObject.get("asc", JsonValue.TRUE).getValue(Boolean.class);

            sortOrderQuery.append("JSON_VALUE(document, '$." + field + "')");

            if (ascending) {
                sortOrderQuery.append(" ASC, ");
            } else {
                sortOrderQuery.append(" DESC, ");
            }
        }

        sortOrderQuery.delete(sortOrderQuery.length() - 2, sortOrderQuery.length());

        return sortOrderQuery.toString();
    }
}
