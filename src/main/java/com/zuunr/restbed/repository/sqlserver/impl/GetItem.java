/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObjectSupport;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.repository.ReactiveRepository;
import com.zuunr.restbed.repository.sqlserver.util.CollectionNameProvider;
import com.zuunr.restbed.repository.sqlserver.util.ResourceDeserializer;
import com.zuunr.restbed.repository.sqlserver.util.SchemaNameProvider;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * <p>Primary getItem implementation of {@link ReactiveRepository}.</p>
 * 
 * @see ReactiveRepository
 */
@Component
public class GetItem<T extends JsonObjectSupport> {

    private final Logger logger = LoggerFactory.getLogger(GetItem.class);

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private CollectionNameProvider collectionNameProvider;
    private SchemaNameProvider schemaNameProvider;
    private ResourceDeserializer resourceDeserializer;

    @Autowired
    public GetItem(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate,
            CollectionNameProvider collectionNameProvider,
            SchemaNameProvider schemaNameProvider,
            ResourceDeserializer resourceDeserializer) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.collectionNameProvider = collectionNameProvider;
        this.schemaNameProvider = schemaNameProvider;
        this.resourceDeserializer = resourceDeserializer;
    }

    /**
     * @see ReactiveRepository#getItem(Exchange, Class)
     */
    public Mono<Exchange<T>> getItem(Exchange<T> exchange, Class<T> resourceClass) {
        String collectionName = collectionNameProvider.getCollectionName(exchange.getRequest());
        String schemaName = schemaNameProvider.getSchemaName();
        String id = exchange.getRequest().getApiUriInfo().itemId();

        StringBuilder sql = new StringBuilder()
                .append("SELECT document FROM " + schemaName + "." + collectionName)
                .append(" WHERE JSON_VALUE(document, '$.id') = :id");
        MapSqlParameterSource parameters = new MapSqlParameterSource("id", id);

        return Mono.fromCallable(() -> queryForDocument(sql.toString(), parameters))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(o -> successGet(exchange, o, resourceClass))
                .onErrorReturn(EmptyResultDataAccessException.class, emptyGet(exchange));
    }

    private String queryForDocument(String sql, MapSqlParameterSource parameters) {
        return namedParameterJdbcTemplate.queryForObject(sql, parameters, String.class);
    }

    private Exchange<T> emptyGet(Exchange<T> originalExchange) {
        return originalExchange.response(Response.<T>create(StatusCode._404_NOT_FOUND));
    }

    private Mono<Exchange<T>> successGet(Exchange<T> exchange, String fetchedDocument, Class<T> resourceClass) {
        T body = resourceDeserializer.deserializeJson(fetchedDocument, resourceClass);

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully retrieved item: {}", exchange.getRequest().getUrl());
        }

        return Mono.just(exchange
                .response(Response.<T>create(StatusCode._200_OK)
                        .body(body)));
    }
}
