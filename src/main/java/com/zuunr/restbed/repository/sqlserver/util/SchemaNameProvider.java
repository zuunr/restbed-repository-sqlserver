/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.restbed.repository.sqlserver.config.Config;

/**
 * <p>The SchemaNameProvider is responsible for retrieving the 
 * schema name used for this SQL server.</p>
 * 
 * @author Mikael Ahlberg
 */
@Component
public class SchemaNameProvider {

    private Config config;

    @Autowired
    public SchemaNameProvider(Config config) {
        this.config = config;
    }

    /**
     * <p>Returns the schema name for the SQL server.</p>
     * 
     * @return the schema name
     */
    public String getSchemaName() {
        return config.getSchemaName();
    }
}
