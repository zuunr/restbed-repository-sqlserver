/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.restbed.core.exchange.Collection;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.repository.ReactiveRepository;
import com.zuunr.restbed.repository.sqlserver.config.Config;
import com.zuunr.restbed.repository.sqlserver.filter.FilterQueryProvider;
import com.zuunr.restbed.repository.sqlserver.sort.SortOrderProvider;
import com.zuunr.restbed.repository.sqlserver.util.CollectionNameProvider;
import com.zuunr.restbed.repository.sqlserver.util.ResourceDeserializer;
import com.zuunr.restbed.repository.sqlserver.util.SchemaNameProvider;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * <p>Primary getCollection implementation of {@link ReactiveRepository}.</p>
 * 
 * @see ReactiveRepository
 */
@Component
public class GetCollection<T extends JsonObjectSupport> {

    private final Logger logger = LoggerFactory.getLogger(GetCollection.class);

    private static final int DEFAULT_OFFSET = 0;

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private CollectionNameProvider collectionNameProvider;
    private SchemaNameProvider schemaNameProvider;
    private FilterQueryProvider filterQueryProvider;
    private SortOrderProvider sortOrderProvider;
    private Config config;
    private ResourceDeserializer resourceDeserializer;

    @Autowired
    public GetCollection(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate,
            CollectionNameProvider collectionNameProvider,
            SchemaNameProvider schemaNameProvider,
            FilterQueryProvider filterQueryProvider,
            SortOrderProvider sortOrderProvider,
            Config config,
            ResourceDeserializer resourceDeserializer) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.collectionNameProvider = collectionNameProvider;
        this.schemaNameProvider = schemaNameProvider;
        this.filterQueryProvider = filterQueryProvider;
        this.sortOrderProvider = sortOrderProvider;
        this.config = config;
        this.resourceDeserializer = resourceDeserializer;
    }

    /**
     * @see ReactiveRepository#getCollection(Exchange, Class)
     */
    public Mono<Exchange<Collection<T>>> getCollection(Exchange<Collection<T>> exchange, Class<T> resourceClass) {
        String collectionName = collectionNameProvider.getCollectionName(exchange.getRequest());
        String schemaName = schemaNameProvider.getSchemaName();

        JsonArray filter = exchange.getRequest().getApiUriInfo().filter();
        JsonArray sortOrder = exchange.getRequest().getApiUriInfo().sortOrder();

        int offset = getOffset(exchange);
        int limit = getLimit(exchange);

        StringBuilder sql = new StringBuilder()
                .append("SELECT document FROM " + schemaName + "." + collectionName)
                .append(" " + filterQueryProvider.getFilterQuery(filter))
                .append(" " + sortOrderProvider.getSortOrderQuery(sortOrder))
                .append(" OFFSET :offset ROWS")
                .append(" FETCH NEXT :limit ROWS ONLY");
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("offset", offset)
                .addValue("limit", limit);

        return Mono.fromCallable(() -> queryForDocuments(sql.toString(), parameters))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMapIterable(docs -> docs)
                .flatMapSequential(o -> deserialize(o, resourceClass))
                .collectList()
                .map(o -> successGetCollection(exchange, o, offset, limit));
    }

    private List<String> queryForDocuments(String sql, MapSqlParameterSource parameters) {
        return namedParameterJdbcTemplate.queryForList(sql, parameters, String.class);
    }

    private Mono<T> deserialize(String fetchedDocument, Class<T> resourceClass) {
        return Mono.just(resourceDeserializer.deserializeJson(fetchedDocument, resourceClass));
    }

    private Exchange<Collection<T>> successGetCollection(Exchange<Collection<T>> exchange, List<T> fetchedItems, int offset, int limit) {
        Collection<T> collection = Collection.<T>builder()
                .offset(offset)
                .limit(limit)
                .value(fetchedItems)
                .href(exchange.getRequest().getApiUriInfo().uri())
                .size(fetchedItems.size())
                .build();

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully retrieved collection: {}", exchange.getRequest().getUrl());
        }

        return exchange.response(Response.<Collection<T>>create(StatusCode._200_OK)
                .body(collection));
    }

    private int getOffset(Exchange<Collection<T>> exchange) {
        return Optional.ofNullable(exchange.getRequest().getApiUriInfo().offset())
                .orElse(DEFAULT_OFFSET);
    }

    private int getLimit(Exchange<Collection<T>> exchange) {
        return Optional.ofNullable(exchange.getRequest().getApiUriInfo().limit())
                .orElse(config.getDefaultGetCollectionLimit());
    }
}
