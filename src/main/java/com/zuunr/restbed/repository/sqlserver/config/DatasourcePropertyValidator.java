/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.config;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

/**
 * <p>This class makes sure that any spring datasource property,
 * not validated by spring, is validated when running.</p>
 * 
 * <p>Properties in this class should not be used.</p>
 * 
 * @author Mikael Ahlberg
 */
@ConfigurationProperties(prefix = "spring.datasource")
@Validated
class DatasourcePropertyValidator {

    @NotEmpty(message = "You must set the database url")
    private final String url;
    @NotEmpty(message = "You must set the user to use for the database")
    private final String username;
    @NotEmpty(message = "You must set the password to use for the database")
    private final String password;
    
    @ConstructorBinding
    public DatasourcePropertyValidator(
            String url,
            String username,
            String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }
}
