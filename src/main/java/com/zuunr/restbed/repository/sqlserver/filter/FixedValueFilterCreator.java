/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.filter;

import org.springframework.stereotype.Component;

/**
 * <p>The FixedValueFilterCreator is responsible for creating and setting up
 * a json-friendly SQL string for fixed value filtering, e.g. x = 42 or x = 100.</p>
 * 
 * <ul>
 * <li>JSON_VALUE(document, '$.myParam') = 42</li>
 * <li>(JSON_VALUE(document, '$.myParam') = 42 OR JSON_VALUE(document, '$.myParam') = 100)</li>
 * </ul>
 *
 * @author Mikael Ahlberg
 */
@Component
public class FixedValueFilterCreator {
    
    /**
     * <p>Create a new SQL string which is either equals to the provided
     * value, or by using an OR operator, can be any of the provided values.</p>
     * 
     * @param filterExpression contains the fixed expression values
     * @param path is the value to apply the filter on
     * @return a string containing the SQL filter query
     */
    public String createFilter(String filterExpression, String path) {
        if (filterExpression.contains(",")) {
            return createMultiFixedValueFilter(filterExpression, path);
        }
        
        return "JSON_VALUE(document, '$." + path + "') = " + filterExpression;
    }
    
    private String createMultiFixedValueFilter(String filterExpression, String path) {
        StringBuilder fixedValuesQuery = new StringBuilder();
        fixedValuesQuery.append("(");
        
        for (String fixedValue : filterExpression.split(",")) {
            fixedValuesQuery.append("JSON_VALUE(document, '$." + path + "') = " + fixedValue + " OR ");
        }
        
        fixedValuesQuery.delete(fixedValuesQuery.length() - 4, fixedValuesQuery.length());
        fixedValuesQuery.append(")");
        
        return fixedValuesQuery.toString();
    }
}
