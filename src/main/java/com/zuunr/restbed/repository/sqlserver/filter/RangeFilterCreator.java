/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.filter;

import java.util.Optional;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

/**
 * <p>The RangeFilterCreator is responsible for creating and setting up a
 * json-friendly SQL string for range filtering, e.g. x &gt;= 10 and x &lt; 20.</p>
 * 
 * <ul>
 * <li>JSON_VALUE(document, '$.myParam') &lt; 10</li>
 * <li>JSON_VALUE(document, '$.myParam') &gt;= 10 AND JSON_VALUE(document, '$.myParam') &lt; 20</li>
 * </ul>
 *
 * @author Mikael Ahlberg
 */
@Component
public class RangeFilterCreator {
    
    public static final Pattern pattern = Pattern.compile("([\\[\\(])([^\\,]*),([^\\)\\]]*)([\\]\\)])");

    private static final String INCLUSIVE_START_TAG = "[";
    private static final String INCLUSIVE_END_TAG = "]";
    
    /**
     * <p>Create a new range SQL string with {less than, greater than, less or equals than
     * and greater or equals than} the provided values in form of a {@link MatchResult}.</p>
     * 
     * @param matcher contains the filter expression
     * @param path is the value to apply the filter on
     * @return a string containing the SQL filter query
     */
    public String createFilter(MatchResult matcher, String path) {
        try {
            String start = matcher.group(1);
            boolean inclusiveStart = start.equals(INCLUSIVE_START_TAG);
            String end = matcher.group(4);
            boolean inclusiveEnd = end.equals(INCLUSIVE_END_TAG);

            String intervalStart = matcher.group(2);
            String intervalEnd = matcher.group(3);

            String intervalStartQuery = addIntervalStart(inclusiveStart, intervalStart);
            String intervalEndQuery = addIntervalEnd(inclusiveEnd, intervalEnd);

            if (intervalStartQuery != null && intervalEndQuery != null) {
                return "JSON_VALUE(document, '$." + path + "') " + intervalStartQuery
                        + " AND JSON_VALUE(document, '$." + path + "') " + intervalEndQuery;
            } else {
                return "JSON_VALUE(document, '$." + path + "') " + Optional.ofNullable(intervalStartQuery).orElse(intervalEndQuery);
            }
        } catch (Exception e) {
            throw new ApplyFilterException("An error occured when creating new filter query", e);
        }
    }

    private String addIntervalStart(boolean inclusiveStart, String intervalStart) {
        if (intervalStart.isEmpty()) {
            return null;
        }

        if (inclusiveStart) {
            return ">= " + intervalStart;
        } else {
            return "> " + intervalStart;
        }
    }

    private String addIntervalEnd(boolean inclusiveEnd, String intervalEnd) {
        if (intervalEnd.isEmpty()) {
            return null;
        }

        if (inclusiveEnd) {
            return "<= " + intervalEnd;
        } else {
            return "< " + intervalEnd;
        }
    }
}
