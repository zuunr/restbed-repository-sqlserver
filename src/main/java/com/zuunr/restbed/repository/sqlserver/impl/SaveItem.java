/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.restbed.core.exchange.Exchange;
import com.zuunr.restbed.core.exchange.Response;
import com.zuunr.restbed.core.exchange.StatusCode;
import com.zuunr.restbed.core.service.util.EtagProvider;
import com.zuunr.restbed.repository.ReactiveRepository;
import com.zuunr.restbed.repository.sqlserver.util.CollectionNameProvider;
import com.zuunr.restbed.repository.sqlserver.util.ResourceDeserializer;
import com.zuunr.restbed.repository.sqlserver.util.SchemaNameProvider;

import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * <p>Primary saveItem implementation of {@link ReactiveRepository}.</p>
 * 
 * @see ReactiveRepository
 */
@Component
public class SaveItem<T extends JsonObjectSupport> {

    private final Logger logger = LoggerFactory.getLogger(SaveItem.class);

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private CollectionNameProvider collectionNameProvider;
    private SchemaNameProvider schemaNameProvider;
    private EtagProvider etagProvider;
    private ResourceDeserializer resourceDeserializer;

    @Autowired
    public SaveItem(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate,
            CollectionNameProvider collectionNameProvider,
            SchemaNameProvider schemaNameProvider,
            EtagProvider etagProvider,
            ResourceDeserializer resourceDeserializer) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.collectionNameProvider = collectionNameProvider;
        this.schemaNameProvider = schemaNameProvider;
        this.etagProvider = etagProvider;
        this.resourceDeserializer = resourceDeserializer;
    }

    /**
     * @see ReactiveRepository#saveItem(Exchange, Class)
     */
    public Mono<Exchange<T>> saveItem(Exchange<T> exchange, Class<T> resourceClass) {
        String collectionName = collectionNameProvider.getCollectionName(exchange.getRequest());
        String schemaName = schemaNameProvider.getSchemaName();

        JsonObject body = exchange.getRequest().getBodyAsJsonObject();
        JsonObject bodyWithEtag = etagProvider.appendEtag(body);

        String sql = "INSERT INTO " + schemaName + "." + collectionName + " (document) values (:document)";
        MapSqlParameterSource parameters = new MapSqlParameterSource("document", bodyWithEtag.asJson());

        return Mono.fromCallable(() -> insertDocument(sql, parameters))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(o -> successSave(exchange, bodyWithEtag, resourceClass));
    }

    private int insertDocument(String sql, MapSqlParameterSource parameters) {
        return namedParameterJdbcTemplate.update(sql, parameters);
    }

    private Mono<Exchange<T>> successSave(Exchange<T> exchange, JsonObject savedDocument, Class<T> resourceClass) {
        T body = resourceDeserializer.deserializeJsonObject(savedDocument, resourceClass);

        if (logger.isDebugEnabled()) {
            logger.debug("Successfully saved item: {}", exchange.getRequest().getUrl());
        }

        return Mono.just(exchange
                .response(Response.<T>create(StatusCode._201_CREATED)
                        .body(body)));
    }
}
