/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.impl.model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * <p>NoResourceDeserializer returns an EMPTY {@link NoResource} whenever
 * it is used for deserializing.</p>
 * 
 * @author Mikael Ahlberg
 */
public class NoResourceDeserializer extends StdDeserializer<NoResource> {

    private static final long serialVersionUID = 1L;

    public NoResourceDeserializer() {
        this(null);
    }

    protected NoResourceDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public NoResource deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return NoResource.EMPTY;
    }
}
