/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.filter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class FixedValueFilterCreatorTest {
    
    private FixedValueFilterCreator fixedValueFilterCreator = new FixedValueFilterCreator();
    
    @Test
    void givenFixedValueShouldCreateSQL() {
        String filterCriteria = fixedValueFilterCreator.createFilter("42", "myParam");
        
        assertEquals("JSON_VALUE(document, '$.myParam') = 42", filterCriteria);
    }
    
    @Test
    void givenFixedValuesShouldCreateSQL() {
        String filterCriteria = fixedValueFilterCreator.createFilter("42,100,30,25", "myParam");
        
        assertEquals("(JSON_VALUE(document, '$.myParam') = 42 OR JSON_VALUE(document, '$.myParam') = 100 OR JSON_VALUE(document, '$.myParam') = 30 OR JSON_VALUE(document, '$.myParam') = 25)", filterCriteria);
    }
}
