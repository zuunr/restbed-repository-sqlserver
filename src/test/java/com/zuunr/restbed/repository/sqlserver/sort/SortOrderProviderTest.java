/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.sort;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

class SortOrderProviderTest {

    private SortOrderProvider sortOrderProvider = new SortOrderProvider();

    @Test
    void givenNoSortOrderShouldReturnOrderById() {
        String sortOrderQuery = sortOrderProvider.getSortOrderQuery(JsonArray.EMPTY);

        assertEquals("ORDER BY _id", sortOrderQuery);
    }

    @Test
    void givenOneParameterAscendingShouldCreateAscendingOrder() {
        JsonObject parameter = JsonObject.EMPTY
                .put("param", "myParam")
                .put("asc", JsonValue.TRUE);

        String sortOrderQuery = sortOrderProvider.getSortOrderQuery(JsonArray.of(parameter));

        assertEquals("ORDER BY JSON_VALUE(document, '$.myParam') ASC", sortOrderQuery);
    }

    @Test
    void givenOneParameterDescendingShouldCreateDescendingOrder() {
        JsonObject parameter = JsonObject.EMPTY
                .put("param", "myParam")
                .put("asc", JsonValue.FALSE);

        String sortOrderQuery = sortOrderProvider.getSortOrderQuery(JsonArray.of(parameter));

        assertEquals("ORDER BY JSON_VALUE(document, '$.myParam') DESC", sortOrderQuery);
    }

    @Test
    void givenOneParameterWithNoOrderShouldCreateAscendingOrder() {
        JsonObject parameter = JsonObject.EMPTY
                .put("param", "myParam");

        String sortOrderQuery = sortOrderProvider.getSortOrderQuery(JsonArray.of(parameter));

        assertEquals("ORDER BY JSON_VALUE(document, '$.myParam') ASC", sortOrderQuery);
    }

    @Test
    void givenMultiParametersShouldCreateOrder() {
        JsonObject parameter1 = JsonObject.EMPTY
                .put("param", "myParam1");
        JsonObject parameter2 = JsonObject.EMPTY
                .put("param", "myParam2")
                .put("asc", JsonValue.FALSE);

        String sortOrderQuery = sortOrderProvider.getSortOrderQuery(JsonArray.of(parameter1, parameter2));

        assertEquals("ORDER BY JSON_VALUE(document, '$.myParam1') ASC, JSON_VALUE(document, '$.myParam2') DESC", sortOrderQuery);
    }
}
