/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.filter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.regex.MatchResult;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

class FilterQueryProviderTest {

    private FilterQueryProvider filterQueryProvider = new FilterQueryProvider(new MyRangeFilterCreator(), new MyFixedValueFilterCreator());

    @Test
    void givenNoFilterShouldReturnEmptyQuery() {
        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.EMPTY);

        assertEquals("", filterQuery);
    }

    @Test
    void givenRangeValuesShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "(10,42)");

        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.of(filter));

        assertEquals("WHERE rangeValue", filterQuery);
    }

    @Test
    void givenInclusiveRangeValuesShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "[10,42]");

        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.of(filter));

        assertEquals("WHERE rangeValue", filterQuery);
    }

    @Test
    void givenOneRangeValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "(10,)");

        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.of(filter));

        assertEquals("WHERE rangeValue", filterQuery);
    }

    @Test
    void givenOneInclusiveRangeValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "[10,]");

        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.of(filter));

        assertEquals("WHERE rangeValue", filterQuery);
    }

    @Test
    void givenSecondRangeValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "(,42)");

        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.of(filter));

        assertEquals("WHERE rangeValue", filterQuery);
    }

    @Test
    void givenSecondInclusiveRangeValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "[,42]");

        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.of(filter));

        assertEquals("WHERE rangeValue", filterQuery);
    }
    
    @Test
    void givenFixedValueShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "42");

        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.of(filter));

        assertEquals("WHERE fixedValue", filterQuery);
    }
    
    @Test
    void givenFixedValuesShouldCreateQuery() {
        JsonObject filter = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "42,10,30");

        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.of(filter));

        assertEquals("WHERE fixedValue", filterQuery);
    }
    
    @Test
    void givenMultipleParametersShouldCreateQuery() {
        JsonObject filter1 = JsonObject.EMPTY
                .put("path", "myParam")
                .put("expr", "42,10,30");
        JsonObject filter2 = JsonObject.EMPTY
                .put("path", "myOtherParam")
                .put("expr", "[,42]");

        String filterQuery = filterQueryProvider.getFilterQuery(JsonArray.of(filter1, filter2));

        assertEquals("WHERE fixedValue AND rangeValue", filterQuery);
    }
    
    private class MyRangeFilterCreator extends RangeFilterCreator {
        @Override
        public String createFilter(MatchResult matcher, String path) {
            return "rangeValue";
        }
    }
    
    private class MyFixedValueFilterCreator extends FixedValueFilterCreator {
        @Override
        public String createFilter(String filterExpression, String path) {
            return "fixedValue";
        }
    }
}
