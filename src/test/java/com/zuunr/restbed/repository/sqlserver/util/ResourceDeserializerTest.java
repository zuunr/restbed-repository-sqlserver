/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.bson.Document;
import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;

class ResourceDeserializerTest {

    private ResourceDeserializer resourceDeserializer = new ResourceDeserializer();

    @Test
    void givenDocumentObjectShouldConvertToResource() {
        final BigDecimal amount = new BigDecimal("120.00");
        Document document = new Document();
        document.put("id", "123456");
        document.put("amount", amount);

        JsonObjectWrapper deserializedObject = resourceDeserializer.deserialize(document, JsonObjectWrapper.class);

        assertEquals(2, deserializedObject.asJsonObject().size());
        assertEquals("123456", deserializedObject.asJsonObject().get("id").as(String.class));
        assertEquals(amount, deserializedObject.asJsonObject().get("amount").getBigDecimal());
    }

    @Test
    void givenJsonObjectShouldConvertToResource() {
        final BigDecimal amount = new BigDecimal("120.00");
        JsonObject jsonObject = JsonObject.EMPTY
                .put("id", "123456")
                .put("amount", amount);

        JsonObjectWrapper deserializedObject = resourceDeserializer.deserializeJsonObject(jsonObject, JsonObjectWrapper.class);

        assertEquals(2, deserializedObject.asJsonObject().size());
        assertEquals("123456", deserializedObject.asJsonObject().get("id").as(String.class));
        assertEquals(amount, deserializedObject.asJsonObject().get("amount").getBigDecimal());
    }

    @Test
    void givenJsonStringShouldConvertToResource() {
        final BigDecimal amount = new BigDecimal("120.00");
        String json = JsonObject.EMPTY
                .put("id", "123456")
                .put("amount", amount)
                .asJson();

        JsonObjectWrapper deserializedObject = resourceDeserializer.deserializeJson(json, JsonObjectWrapper.class);

        assertEquals(2, deserializedObject.asJsonObject().size());
        assertEquals("123456", deserializedObject.asJsonObject().get("id").as(String.class));
        assertEquals(amount, deserializedObject.asJsonObject().get("amount").getBigDecimal());
    }

    @Test
    void givenCanNotConvertShouldThrowException() {
        Document document = new Document();
        document.put("id", "123456");

        assertThrows(DeserializationException.class, () -> {
            resourceDeserializer.deserialize(document, String.class);
        });
    }
}
