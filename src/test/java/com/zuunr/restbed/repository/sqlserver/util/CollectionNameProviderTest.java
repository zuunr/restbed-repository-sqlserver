/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.repository.sqlserver.config.Config;

class CollectionNameProviderTest {

    @Test
    void givenNoPrefixShouldReturnOnlyCollectionName() {
        Config config = new Config(false, 0, "schemaName");
        String collectionName = new CollectionNameProvider(config).getCollectionName(createRequest());

        assertEquals("thecollection", collectionName);
    }

    @Test
    void givenPrefixShouldReturnCollectionNameWithApiName() {
        Config config = new Config(true, 0, "schemaName");
        String collectionName = new CollectionNameProvider(config).getCollectionName(createRequest());

        assertEquals("myapi-thecollection", collectionName);
    }

    private Request<?> createRequest() {
        return Request.create(Method.GET)
                .url("http://somedomain.com/v1/myapi/api/thecollection");
    }
}
