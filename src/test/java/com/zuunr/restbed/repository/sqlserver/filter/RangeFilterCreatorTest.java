/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.repository.sqlserver.filter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.regex.Matcher;

import org.junit.jupiter.api.Test;

class RangeFilterCreatorTest {

    private RangeFilterCreator rangeFilterCreator = new RangeFilterCreator();
    
    @Test
    void givenRangeValuesShouldCreateSQL() {
        String filterCriteria = rangeFilterCreator.createFilter(initMatcher("(10,42)"), "myParam");
        
        assertEquals("JSON_VALUE(document, '$.myParam') > 10 AND JSON_VALUE(document, '$.myParam') < 42", filterCriteria);
    }

    @Test
    void givenInclusiveRangeValuesShouldCreateSQL() {
        String filterCriteria = rangeFilterCreator.createFilter(initMatcher("[10,42]"), "myParam");
        
        assertEquals("JSON_VALUE(document, '$.myParam') >= 10 AND JSON_VALUE(document, '$.myParam') <= 42", filterCriteria);
    }

    @Test
    void givenOneRangeValueShouldCreateSQL() {
        String filterCriteria = rangeFilterCreator.createFilter(initMatcher("(10,)"), "myParam");
        
        assertEquals("JSON_VALUE(document, '$.myParam') > 10", filterCriteria);
    }

    @Test
    void givenOneInclusiveRangeValueShouldCreateSQL() {
        String filterCriteria = rangeFilterCreator.createFilter(initMatcher("[10,]"), "myParam");
        
        assertEquals("JSON_VALUE(document, '$.myParam') >= 10", filterCriteria);
    }

    @Test
    void givenSecondRangeValueShouldCreateSQL() {
        String filterCriteria = rangeFilterCreator.createFilter(initMatcher("(,42)"), "myParam");
        
        assertEquals("JSON_VALUE(document, '$.myParam') < 42", filterCriteria);
    }

    @Test
    void givenSecondInclusiveRangeValueShouldCreateSQL() {
        String filterCriteria = rangeFilterCreator.createFilter(initMatcher("[,42]"), "myParam");
        
        assertEquals("JSON_VALUE(document, '$.myParam') <= 42", filterCriteria);
    }
    
    private Matcher initMatcher(String regex) {
        Matcher matcher = RangeFilterCreator.pattern.matcher(regex);
        matcher.matches();
        
        return matcher;
    }
}
