# restbed-repository-sqlserver

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr.restbed/restbed-repository-sqlserver.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=a:restbed-repository-sqlserver)
[![Snyk](https://snyk-widget.herokuapp.com/badge/mvn/com.zuunr.restbed/restbed-repository-sqlserver/badge.svg)](https://snyk.io/vuln/maven:com.zuunr.restbed%3Arestbed-repository-sqlserver)

This is the reactive SQL Server implementation for Restbed modules. It is merely a reactive wrapper, and the module should not be regarded as a reactive SQL module since it contains blocking IO. The module simply offloads the blocking IO to the elastic thread pool.

## Supported tags

* [`1.0.0.M7-59f711a`, (*59f711a/pom.xml*)](https://bitbucket.org/zuunr/restbed-repository-sqlserver/src/59f711a/pom.xml)

## Usage

To use this module, make sure this project's maven artifact is added as a dependency in your module, replacing 1.0.0-abcdefg with a supported tag:

```xml
<dependency>
	<groupId>com.zuunr.restbed</groupId>
	<artifactId>restbed-repository-sqlserver</artifactId>
	<version>1.0.0-abcdefg</version>
</dependency>
```

## Configuration

The module requires additional configuration, see src/test/resources/application.yml for a sample.

Remember to use environment variables for database access properties instead of the application.yml file in a non-test environment.

## Database tables and schemas

All collections must be pre-created in the database before first use.

```sql
# Step one is to create a documentdb database	
create database documentdb

# Step two is to create a new collections schema on that database
create schema collections

# Step three is to create the collection table on that schema (e.g. /accounts or /players) with the following columns 
# (e.g. replacing thecollection with your collection name)
create table collections.thecollection (
    _id bigint primary key IDENTITY,
    document NVARCHAR(max)
);
```

You are now ready to go!

## Indexes

Database indexes must be created manually.

```sql
create table collections.players (
    _id bigint primary key IDENTITY,
    document NVARCHAR(max),
    
    jsonPlayerName AS JSON_VALUE(document, '$.playerName'),
    index ix_json_player_name (jsonPlayerName)
);
```

## Integration tests

To run the integration tests a SQL Server runtime must be started. Issue the following docker command to start a local container based SQL Server:

First create the shared docker network:

```shell
docker network create restbed_default
```

Then start the sql server:

```shell
docker-compose up
```

After that the ReactiveSqlServerRepositoryIT can be executed, given that the documentdb database and the table named **thecollection** has been created.
